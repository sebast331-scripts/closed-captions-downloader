import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, SimpleChanges, OnChanges } from '@angular/core';
import { CaptionService } from '../caption.service';
import captionSources, { ICaptionSource } from '../caption.service.config';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() onFollowChange: EventEmitter<boolean> = new EventEmitter();
  @Output() onFileChange: EventEmitter<string> = new EventEmitter();
  fileName: String = 'cbcnews.txt';
  sources: ICaptionSource[] = captionSources

  constructor() { }

  ngOnInit(): void {
  }

  setFileName(fileName): void {
    this.fileName = fileName;
    this.onFileChange.emit(fileName);
  }

}
