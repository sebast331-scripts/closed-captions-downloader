import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'ccformatter'
})
export class CCFormatterPipe implements PipeTransform {

  constructor (protected sanitizer: DomSanitizer) {}

  transform(value: string): SafeHtml {
    let newString = value;
    // Sentence case
    newString = newString.toLowerCase().replace(/[a-z]/i, (letter) => letter.toUpperCase()).trim();
    // Add <br> before >> and [...] symbols
    newString = newString.replace(/>>/g, '<br><br> >>');
    newString = newString.replace(/\[/g, '<br><br> [');
    newString = newString.replace(/- /g, '<br><br> - ');
    newString = newString.replace(/[\r\n]/g, ' ');
    newString = newString.replace(/[\.:>] \w/g, (a) => a.toUpperCase());
    newString = newString.replace(/[\.:>]  \w/g, (a) => a.toUpperCase());
    newString = newString.replace(/[\.:>]   \w/g, (a) => a.toUpperCase());
    newString = newString.replace(/justin trudeau/g, 'Justin Trudeau');
    newString = newString.replace(/ i /g, ' I ');
    newString = newString.replace(/ i'm /g, ' I\'m ');
    newString = newString.replace(/ canada/gi, ' Canada');
    newString = newString.replace(/ canadian/gi, ' Canadian');
    newString = newString.replace(/covid-19/gi, 'COVID-19');
    return this.sanitizer.bypassSecurityTrustHtml(newString);
  }

}
