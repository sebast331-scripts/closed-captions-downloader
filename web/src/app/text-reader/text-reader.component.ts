import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CaptionService } from '../caption.service';

@Component({
  selector: 'app-text-reader',
  templateUrl: './text-reader.component.html',
  styleUrls: ['./text-reader.component.css']
})
export class TextReaderComponent implements OnInit, OnChanges {
  caption = 'Hello World';
  @Input() followActive: boolean;
  @Input() fileName: String;

  constructor(private captionService: CaptionService) { }

  ngOnInit(): void {
    this.getData();
    setInterval(() => this.getData(), 1000);

    // Check required
    if (this.fileName === null)
      throw new Error("Attribute 'fileName' is required");

  }

  ngOnChanges(change: SimpleChanges): void {
    if ('fileName' in change) {
      this.getData();
    }
  }

  getData(): void {
    this.captionService.getText(this.fileName).subscribe((data) => {
      // console.log(data)
      if (this.followActive) {
        document.getElementById('textReader').scrollIntoView(false);
      }
      this.caption = this.sentenceCase(data);
    })
  }

  sentenceCase(str: string) {
    return str.toLowerCase().replace(/[a-z]/i, (letter) => letter.toUpperCase()).trim();
  }

}
