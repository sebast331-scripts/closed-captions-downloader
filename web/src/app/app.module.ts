import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TextReaderComponent } from './text-reader/text-reader.component';
import { CCFormatterPipe } from './ccformatter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TextReaderComponent,
    CCFormatterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
