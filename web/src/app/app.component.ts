import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'web';
  followActive = false;
  fileName = 'cbcnews.txt';
  textToCapitalize = '';
  capitalizedText = '';

  onFollowChange(isActive) {
    this.followActive = isActive;
  }

  onFileChange(fileName) {
    this.fileName = fileName;
  }
}
