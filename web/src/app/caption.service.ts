import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, Subscriber } from 'rxjs';
import captionSources, { ICaptionSource } from './caption.service.config';

@Injectable({
  providedIn: 'root'
})
export class CaptionService {
  captionUrl = '/assets';

  constructor(private http: HttpClient) { }

  getText(fileName) {
    return this.http.get(`${this.captionUrl}/${fileName}`, {responseType: 'text'});
  }

  getSources(): ICaptionSource[] {
    return captionSources;
  }

}
