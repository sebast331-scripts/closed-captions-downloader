export interface ICaptionSource {
  name: string,
  file: string
}

let captionSources: ICaptionSource[] = [
    {
      name: 'CBC News',
      file: 'cbcnews.txt'
    },
    {
      name: 'ICI RDI',
      file: 'icirdi.txt'
    }
  ]
export default captionSources
