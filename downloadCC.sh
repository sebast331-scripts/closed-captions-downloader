#!/bin/bash

# URL: https://prod-fastly-us-east-1.video.pscp.tv/Transcoding/v1/hls/M6dP2_xKlnhJFPwETPT-QYHehr0NlGkXvUiaxucGMdxHrZxo_xb_doc2WJqk9xg2O8lQODpOFJILdpPfB0tJGA/transcode/us-east-1/periscope-replay-direct-prod-us-east-1-public/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsInZlcnNpb24iOiIyIn0.eyJIZWlnaHQiOjQ4MCwiS2JwcyI6MTAwMCwiV2lkdGgiOjg0OH0.RFzmPpRYVt-M5xPGoWoifZwAR5Wj0QbokQ2FVsxVt9c/dynamic_highlatency.m3u8?type=live
URL=$1
FILENAME=$2

ffmpeg -hide_banner -report -loglevel error -i "$URL" -map 0:v:0 -c:v:0 copy -a53cc 1 -an -sn -f mpegts - | ccextractor - -in=ts -out=txt -stdout --no_progress_bar

# Download video
#ffmpeg -i "$URL[out0+subcc]" $FILENAME.mkv

# Extract subtitles
#ffmpeg -f lavfi -i "movie=$FILENAME.mkv[out0+subcc]" -map s $FILENAME-raw.srt

# REGEX
#cat $FILENAME-raw.srt | grep '<font' | perl -pe 's/.*}//g' | perl -pe 's/<\/font>//g' | perl -pe 's/\R/ /g' > $FILENAME.txt