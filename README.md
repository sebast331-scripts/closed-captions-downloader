# README

## Description
This script downloads a live stream in order to retrieve Closed Captions from it. It also comes with a web application that allows easy viewing of the captions.

## `downloadCC.sh`
Downloads the closed caption from a feed and outputs in the terminal.

### Usage
- View the CCs: `$ ./downloadCC.sh <file_or_url>`
- Output to file: `$ ./downloadCC.sh <url> >  output.txt`
- Output to terminal + file: `$ ./downloadCC.sh <url> | tee output.txt`

### Examples
`$ ./downloadCC.sh "https://www.mystream.com/stream.ts"`

## `start.sh`
Automation script that asks for 2 URLs, download the closed captions and starts a web server using `python`.

### Usage
`$ ./start.sh`

### Example
```
$ ./start.sh
URL for CBC News: <url1>
URL for ICI RDI: <url2>
Serving HTTP on 0.0.0.0 port 8080 (http://0.0.0.0:8080/) ...
```
When the script is started, open a web browser to http://localhost:8080/

![demo](demo.gif "Demo")

# Special thanks
Reddit user /u/OneStatistician : https://www.reddit.com/r/ffmpeg/comments/gutlek/need_help_pipeing_ffmpeg/fslaiqb/