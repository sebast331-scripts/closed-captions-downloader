#!/bin/bash

# Ask the urls
read -p "URL for CBC News: " urlCBC
read -p "URL for ICI RDI: " urlRDI

# Download the feeds
if [ $urlCBC ]; then
	./downloadCC.sh "$urlCBC" > "web/dist/web/assets/cbcnews.txt" &
fi

if [ $urlRDI ]; then
	./downloadCC.sh "$urlRDI" > "web/dist/web/assets/icirdi.txt" &
fi

# Start the web server
python -m http.server --directory "web/dist/web/" 8080

# Kill the downloadCC
killall downloadCC.sh
